/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npm run dev` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npm run deploy` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

export interface Env {
	// Example binding to KV. Learn more at https://developers.cloudflare.com/workers/runtime-apis/kv/
	FINAL_PRODUCT: KVNamespace;
	//
	// Example binding to Durable Object. Learn more at https://developers.cloudflare.com/workers/runtime-apis/durable-objects/
	// MY_DURABLE_OBJECT: DurableObjectNamespace;
	//
	// Example binding to R2. Learn more at https://developers.cloudflare.com/workers/runtime-apis/r2/
	// MY_BUCKET: R2Bucket;
	//
	// Example binding to a Service. Learn more at https://developers.cloudflare.com/workers/runtime-apis/service-bindings/
	// MY_SERVICE: Fetcher;
	//
	// Example binding to a Queue. Learn more at https://developers.cloudflare.com/queues/javascript-apis/
	// MY_QUEUE: Queue;
	DB: D1Database;
}


export default {


	async fetch(request: Request, env: Env,) {


		// Handle Preflight Requests
		if (request.method === 'OPTIONS') {
			return new Response(null, {
			  status: 204,
			  headers: {
				'Access-Control-Allow-Credentials': 'true',
				'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Headers': 'Content-Type',
				'Content-Type': 'application/json'
			  }
			});
		}


		const { pathname } = new URL(request.url);

		// const country = request.headers.get("cf-ipcountry");

		if (pathname === "/download") {

			let txtfile = await env.FINAL_PRODUCT.get("txtproduct")

			const init = {

				headers: {
					"content-type": "text/html; charset=utf-8",
				},
			};


			return new Response(txtfile, init)

		}


		if (pathname === "/db/") {
			// If you did not use `DB` as your binding name, change it here
			const { results } = await env.DB.prepare(
				"insert into LOGS values('products')"
			)
				.all();
			return Response.json(results);
		}

		if (pathname === "/v1/consumers/login") {

			// var new_Date = new Date();

			// var date = new_Date.toLocaleString();

			// const obj: Object = await request.clone().json()

			// type ObjectKey = keyof typeof obj;

			// const name = 'name' as ObjectKey;
			// const key = 'key' as ObjectKey;

			// const nameval = obj[name]
			// const keyval = obj[key]


			// const st = "insert into LOGS values(\"" + nameval + " " + keyval + " " + country + " " + date + "\")"

			// console.log(st)


			// await env.DB.prepare(
			// 	st
			// )
			// 	.all();

			const url = "https://api.flightfactor.site/v1/consumers/login";
			// const url = "https://6037-91-150-13-86.ngrok-free.app/v1/consumers/login";


			/**
			 * gatherResponse awaits and returns a response body as a string.
			 * Use await gatherResponse(..) in an async function to get the response body
			 * @param {Response} response
			 */
			async function gatherResponse(response: any) {
				const { headers } = response;
				const contentType = headers.get("content-type") || "";
				if (contentType.includes("application/json")) {
					return JSON.stringify(await response.json());
				} else if (contentType.includes("application/text")) {
					return response.text();
				} else if (contentType.includes("text/html")) {
					return response.text();
				} else {
					return response.text();
				}
			}


			const init = {
				// body: JSON.stringify(request.body),
				body: request.body,
				// method: "POST", // *GET, POST, PUT, DELETE, etc.
				// mode: "cors", // no-cors, *cors, same-origin
				// // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
				// // credentials: "same-origin", // include, *same-origin, omit
				// headers: {
				// 	"Content-Type": "application/json",
				// 	// 'Content-Type': 'application/x-www-form-urlencoded',
				// },
				// redirect: "follow", // manual, *follow, error
				// referrerPolicy: "no-referrer", 
				
				method: "POST",
				// mode: "no-cors",

				headers: {
					"content-type": "application/json;charset=UTF-8",
				},
			};
			const response = await fetch(url, init);
			const results = await gatherResponse(response);
			return new Response(results, init);

		}


		return new Response(
			"Call /api/beverages to see everyone who works at Bs Beverages"
		);
	},

};
